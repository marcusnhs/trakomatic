    function marcus_working(~,pauseval)
% Requires the Computer Vision System Toolbox
addpath(genpath(pwd))
try
    % Launches webcam object
    vidObj = webcam;
    vidObj.resolution = '640x480';
catch
    disp('Please make sure that a properly recognized webcam is connected and try again.');
    return
end

% Preprocessing options 
preprocessOpts.matchHistograms = true;
preprocessOpts.adjustHistograms = false;
preprocessOpts.targetForHistogramAndResize = ...
    imread('targetFaceHistogram.pgm');
preprocessOpts.targetSize = 100;

% Folder check/creation
targetDirectory = fullfile(fileparts(which(mfilename)),'CapturedFaces');
personNumber = 1;
dirExists = exist(targetDirectory,'dir') == 7;
if dirExists
    refreshOption = 1;
else
    mkdir(targetDirectory);
    refreshOption = 2;
end
if refreshOption == 1
    rmdir(targetDirectory,'s');
    mkdir(targetDirectory)
    mkdir(fullfile(targetDirectory,filesep,['Person' num2str(1)]))
    personNumber = 1;
elseif refreshOption == 2
    tmp = dir(targetDirectory);
    fcn = @(x)ismember(x,{'.','..'});
    tmp = cellfun(fcn,{tmp.name},'UniformOutput',false);
    personNumber = nnz(~[tmp{:}])+1;
    mkdir(fullfile(targetDirectory,filesep,['Person' num2str(personNumber)]))
end

% Figure properties
fdrFig = figure('windowstyle','normal',...
    'name','Learning face features..; Press <ESC> to Stop',...
    'units','normalized',...
    'menubar','none',...
    'position',[0.2 0.1 0.6 0.7],...
    'closerequestfcn',[],...
    'currentcharacter','0',...
    'keypressfcn',@checkForEscape);

% Minimum bounding box size 
QC.minBBSize = 80;

% Create face detector
faceDetector = vision.CascadeObjectDetector;

% Number of images of each person to capture:
nOfEach = 4;

% Number of images to train on
numTrain = 4;

% Between captured frames (allow time for movement/change):
pauseval = 0.2;

% For cropping of captured faces:
bboxPad = 25;
%
captureNumber = 0;
isDone = false;
getAnother = true;

% Obtain cropped images of faces for training.
RGBFrame = snapshot(vidObj);
frameSize = size(RGBFrame);
imgAx = axes('parent',fdrFig,...
    'units','normalized',...
    'position',[0.05 0.45 0.9 0.45]);
imgHndl = imshow(RGBFrame);shg;
disp('Esc to quit!')
if ismember(refreshOption,[1,2]) && getAnother && ~isDone
    while getAnother && double(get(fdrFig,'currentCharacter')) ~= 27
        % If successful, displayFrame will contain the detection box.
        % Otherwise not.
        [displayFrame, success] = capturePreprocessDetectValidateSave;
        if success
            captureNumber = captureNumber + 1;
        end
        set(imgHndl,'CData',displayFrame);
        if captureNumber >= nOfEach
            beep;pause(0.25);
            beep;
            queryForNext;
        end
    end
end

% Training classifier
imgSet = imageSet(targetDirectory,'recursive');
if numel(imgSet) < 2
    error('streamingFaceRecognition: You must capture at least two individuals for this to work!');
    delete(vidObj)
    release(faceDetector)
    delete(fdrFig)
end
nameFolders;
sceneFeatures = trainStackedFaceDetector(imgSet,numTrain);

% Classification on real-time video.
figure(fdrFig)
while double(get(fdrFig,'currentCharacter')) ~= 27 && ~isDone
    bestGuess = '?';
    RGBFrame = snapshot(vidObj);
    grayFrame = rgb2gray(RGBFrame);
    bboxes = faceDetector.step(grayFrame);
    for jj = 1:size(bboxes,1)
        if all(bboxes(jj,3:4) >= QC.minBBSize)
            thisFace = imcrop(grayFrame,bboxes(jj,:));
            if preprocessOpts.matchHistograms
                thisFace = imhistmatch(thisFace,...
                    preprocessOpts.targetForHistogramAndResize);
            end
            if preprocessOpts.adjustHistograms
                thisFace = histeq(thisFace);
            end
            thisFace = imresize(thisFace,size(preprocessOpts.targetForHistogramAndResize));
           
            % Classsification based on minimizing sum of absolute
            % differences between image features.
            bestGuess = predictFace(thisFace,sceneFeatures,numel(imgSet));
            if bestGuess == 0
                bestGuess = '?';
            else
                bestGuess = imgSet(bestGuess).Description;
            end
            %tPredict = toc
            RGBFrame = 	insertObjectAnnotation(RGBFrame, 'rectangle', bboxes(jj,:), bestGuess,'FontSize',48);
        end
    end
    imshow(RGBFrame,'parent',imgAx);drawnow;
    title([bestGuess '?'])
end %while

% On close
delete(vidObj)
release(faceDetector)
delete(fdrFig)


%%
% Nested functions

    function [displayFrame, success, imagePath] = ...
            capturePreprocessDetectValidateSave(varargin)
        % Captures frame from webcam
        RGBFrame = snapshot(vidObj);
        % Defaults:
        displayFrame = RGBFrame;
        success = false;
        imagePath = [];
        grayFrame = rgb2gray(RGBFrame);
        % Preprocessing of video snapshot
        if preprocessOpts.matchHistograms
            grayFrame = imhistmatch(grayFrame,...
                preprocessOpts.targetForHistogramAndResize); 
        end
        if preprocessOpts.adjustHistograms
            grayFrame = histeq(grayFrame);
        end
        preprocessOpts.targetSize = 100;
        % Detect face
        bboxes = faceDetector.step(grayFrame);
        % Validate detected face properties
        if isempty(bboxes)
            return
        end
        if size(bboxes,1) > 1
            disp('Need to train on one face only.');
            return
        end
        if any(bboxes(3:4) < QC.minBBSize)
            disp('Face is too small.');
            return
        end
        success = true;
        displayFrame = insertShape(RGBFrame, 'Rectangle', bboxes,'LineWidth', 5);
        % Save image to directory
        bboxes = bboxes + [-bboxPad -bboxPad 2*bboxPad 2*bboxPad];
        % Make sure crop region is within image
        bboxes = [max(bboxes(1),1) max(bboxes(2),1) min(frameSize(2),bboxes(3)) min(frameSize(2),bboxes(4))];
        faceImg = imcrop(grayFrame,bboxes);
        minImSize = min(size(faceImg));
        thumbSize = preprocessOpts.targetSize/minImSize;
        faceImg = imresize(faceImg,thumbSize);
        
        % Ensuring image sizes are adequate.
        sz = size(faceImg);
        if min(sz) > preprocessOpts.targetSize
            faceImg = faceImg(1:preprocessOpts.targetSize,1:preprocessOpts.targetSize);
        elseif min(sz) < preprocessOpts.targetSize
            faceImg = imresize(faceImg,[preprocessOpts.targetSize,preprocessOpts.targetSize]);
        end
        
        % Stores training images
        imagePath = fullfile(targetDirectory,...
            ['Person' num2str(personNumber)],filesep,['faceImg' num2str(captureNumber) '.png']);
        imwrite(faceImg,imagePath);
        pause(pauseval)
    end 
    
    % Function to close figure on escape
    function checkForEscape(varargin)
        if double(get(gcf,'currentcharacter'))== 27
            isDone = true;
        end
    end 

    % Names and directs images to their respective folders.
    function nameFolders
        subfolders = pathsFromImageSet(imgSet);
        for ii = 1:numel(imgSet)
            subf = subfolders{ii};
            fs = strfind(subf,filesep);
            subf(fs(end)+1:end) = '';
            subf = [subf,'Person' num2str(ii)];
            if ~isequal(subfolders{ii},subf)
                movefile(subfolders{ii},subf);
            end
        end
        imgSet = imageSet(targetDirectory,'recursive');
    end 
    % Prompts user to select if they want to train on a new face
    function queryForNext
        captureAnother = questdlg('Capture new face?','Capture Another?','YES','No','YES');
        if strcmp(captureAnother,'YES')
            personNumber = personNumber + 1;
            captureNumber = 0;
            mkdir(fullfile(targetDirectory,filesep,['Person' num2str(personNumber)]))
        else
            getAnother = false;
        end
    end

end