To run code:

1) Launch marcus_working.m 
2) Run the function.
3) There will need to be only one face in front of the webcam for training purposes.
4) Once training is done, user will be prompted to train on another face. * Minimum of two faces needed to train on.
5) Once satisfied with number of faces trained on, select 'NO' when prompted to train on a new face.
6) Figure will launch with Person1, Person2,...,PersonN labelled on each face in the figure window.