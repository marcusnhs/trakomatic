function sceneFeatures = trainStackedFaceDetector(imgSet,numTrain)
% Defines number of faces in image set
nFaces = numel(imgSet);
% Randomly selects n (numTrain) images from captured images to train on.
trainingPhotosPerPerson = max(numTrain,min([imgSet.Count]));
testSet = select(imgSet,1:trainingPhotosPerPerson);
allIms = [testSet.ImageLocation];

adjustHistograms = false; 
% Using Speeded-Up Robust Features (SURF) algorithm for features
fcnHandle = @(x) detectSURFFeatures(x);

% Rather than match to individual faces or to "person-averaged faces," we
% create "montages" of each training set. Features are "aggregated" in that
% matches are evaluated to whole training sets, rather than to subsets
% thereof.

inds = reshape(1:numel(allIms),[],nFaces);
scenePoints = cell(nFaces,1);
sceneFeatures = cell(nFaces,1);
targetSize = 100;
thumbSize = [targetSize,targetSize];
for ii = 1:nFaces
	% Create montages of each training set of face images:
	trainingImage = createMontage(allIms(inds(:,ii)),...
		'montageSize',[size(inds,1),1],...
		'thumbSize',thumbSize);
	if adjustHistograms
		trainingImage = histeq(trainingImage);%#ok
	end
	scenePoints{ii} = fcnHandle(trainingImage);
	[sceneFeatures{ii}, scenePoints{ii}] = extractFeatures(trainingImage, scenePoints{ii});
end