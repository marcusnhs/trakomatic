function detected = predictFace(testImage,sceneFeatures,nFaces)
% Used for classifying faces in the image
% Sum of absolute difference
metric = 'SAD';

% Method to detect facial features (SURF)
fcnHandle = @(x) detectSURFFeatures(x);

% Obtains bounding box coordinates of face
boxPoints = fcnHandle(testImage);

% Extracting facial features
[boxFeatures, ~] = extractFeatures(testImage, boxPoints);

% Preallocating a matrix that contains the values of features that match
matchMetric = zeros(size(boxFeatures,1),nFaces);

% Goes through each face and matches it to learnt model.
for ii = 1:nFaces
    [~,matchMetric(:,ii)] = matchFeatures(boxFeatures,sceneFeatures{ii},...
        'MaxRatio',1,...
        'MatchThreshold',100,...
        'Metric',metric);
end
% Indexes detected face 
[~,detected] = min(mean(matchMetric));